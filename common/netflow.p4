// REQUIREMENTS
// Parser: standard_parser.p4
// Control Function: NETFLOW_CONTROL_RETURN
// Metadata Fields: LOGICAL_IN_PORT

// Headers

header_type netflow_metadata_t {
	fields {
		src_port : 8;
		dst_port : 8;
	}
}
metadata netflow_metadata_t netflow_metadata;

// Parser

// Field List

field_list ipv4_netflow_fields {
	LOGICAL_IN_PORT;
	ipv4_hdr.src;
	ipv4_hdr.dst;
	ipv4_hdr.proto;
	netflow_metadata.src_port;
	netflow_metadata.dst_port;
}

field_list ipv6_netflow_fields {
	LOGICAL_IN_PORT;
	ether_hdr.ethertype;
	ipv6_hdr.src;
	ipv6_hdr.dst;
	ipv6_hdr.next_hdr;
	netflow_metadata.src_port;
	netflow_metadata.dst_port;
}

// Actions

action add_ipv4_flow() {
	generate_digest(5, ipv4_netflow_fields);
}

action add_ipv6_flow() {
	generate_digest(5, ipv6_netflow_fields);
}

// Registers

// Tables

table ipv4_flow_table {
	reads {
		LOGICAL_IN_PORT        : exact;
        ipv4_hdr.src           : exact;
        ipv4_hdr.dst           : exact;
        ipv4_hdr.proto         : exact;
		netflow_metadata.src_port : exact;
		netflow_metadata.dst_port : exact;
	}
	actions {
		no_action;
		add_ipv4_flow;
	}
    size : 1024;
}

table ipv6_flow_table {
	reads {
		LOGICAL_IN_PORT        : exact;
        ipv6_hdr.src           : exact;
        ipv6_hdr.dst           : exact;
        ipv6_hdr.next_hdr      : exact;
		netflow_metadata.src_port : exact;
		netflow_metadata.dst_port : exact;
	}
	actions {
		no_action;
		add_ipv6_flow;
	}
    size : 1024;
}

// Control

control netflow {
	if (valid(ipv4_hdr)) { apply(ipv4_flow_table); }
	//else if (valid(ipv6_hdr)) { apply(ipv6_flow_table); }
	NETFLOW_CONTROL_RETURN();
}
