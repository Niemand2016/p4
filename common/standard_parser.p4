// REQUIREMENTS
// Parser|Control Function: STANDARD_PARSER_RETURN

// Ethernet II
// 4 Byte Trailer (CRC Checksum) Not Included
header_type ether_hdr_t {
	fields {
		dst       : 48;
		src       : 48;
		ethertype : 16;
	}
}
header ether_hdr_t ether_hdr;

// 802.1Q VLAN Tag
// EtherType: 0x8100
header_type vlan_hdr_t {
    fields {
        pcp       : 3;
        dei       : 1;
        vid       : 12;
        ethertype : 16;
    }
}
// TODO: Declare VLAN header stack

// IPv4 - RFC 791
// EtherType: 0x0800
// Protocol Number: 4
header_type ipv4_hdr_t {
	fields {
		version   : 4;
		ihl       : 4;
		tos       : 8;
		total_len : 16;
		id        : 16;
		flags     : 3;
		offset    : 13;
		ttl       : 8;
		proto     : 8;
		checksum  : 16;
		src       : 32;
		dst       : 32;
		options   : *;
	}
	length     : 4 * ihl; // In Bytes
	max_length : 60; // In Bytes (Min 20)
}
header ipv4_hdr_t ipv4_hdr;

// IPv6 - RFC 2460
// EtherType: 0x86dd
// Protocol Number: 41
header_type ipv6_hdr_t {
	fields {
		version       : 4;
		traffic_class : 8;
		flow_label    : 20;
		payload_len   : 16;
		next_hdr      : 8;
		hop_limit     : 8;
		src           : 128;
		dst           : 128;
	}
}
header ipv6_hdr_t ipv6_hdr;

// TCP - RFC 793
// Protocol Number: 6
header_type tcp_hdr_t {
	fields {
		src      : 16;
		dst      : 16;
		seq_num  : 32;
		ack_num  : 32;
		offset   : 4;
		reserved : 6;
		flags    : 6;
		window   : 16;
		checksum : 16;
		urg_ptr  : 16;
		options  : *;
	}
	length     : 4 * offset;
	max_length : 60; // Min 20
}
header tcp_hdr_t tcp_hdr;

// UDP - RFC 768
// Protocol Number: 17
header_type udp_hdr_t {
	fields {
		src      : 16;
		dst      : 16;
		len      : 16;
		checksum : 16;
	}
}
header udp_hdr_t udp_hdr;

// Parser

parser_exception pe_unsupported_protocol {
    parser_drop;
}

parser standard_parser {
	return ethernet;
}

parser ethernet {
	extract(ether_hdr);
	return select(ether_hdr.ethertype) {
		0x0800  : ipv4;
		0x86dd  : ipv6;
//		0x88b5  : Experimental 1
//		0x88b6  : Experimental 2
		default : parse_error pe_unsupported_protocol;
	}
}

parser ipv4 {
	extract(ipv4_hdr);
	return select(ipv4_hdr.proto) {
		0x06    : tcp;
		0x11    : udp;
//		0x84    : sctp;
//		0xfd    : Experimental 1
//		0xfe    : Experimental 2
		default : STANDARD_PARSER_RETURN;
	}
}

parser ipv6 {
	extract(ipv6_hdr);
	return select(ipv6_hdr.next_hdr) {
		0x06    : tcp;
		0x11    : udp;
//		0x84    : sctp;
//		0xfd    : Experimental 1
//		0xfe    : Experimental 2
		default : STANDARD_PARSER_RETURN;
	}
}

parser tcp {
	extract(tcp_hdr);
	return STANDARD_PARSER_RETURN;
}

parser udp {
	extract(udp_hdr);
	return STANDARD_PARSER_RETURN;
}
