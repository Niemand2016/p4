#define TIMESTAMP_FIELD intrinsic_metadata.ingress_global_timestamp
#define TIMESTAMP_SIZE 64

header_type intrinsic_metadata_t {
    fields {
        ingress_global_timestamp : TIMESTAMP_SIZE;
    }
}
metadata intrinsic_metadata_t intrinsic_metadata;
