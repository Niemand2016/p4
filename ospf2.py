import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

def find_shortest_paths(start_node, end_node, nodes, edges):
    visited = set()
    tree = set()
    visited.add(start_node)
    nodes[start_node] = 0

    while len(visited) < len(nodes):
        canidate_edges = [(src,dst) for src,dst in edges if src in visited and dst not in visited]
        if len(canidate_edges) == 0 or end_point in visited: break
        src,dst = min(canidate_edges, key=lambda e: nodes[e[0]] + edges[(e[0],e[1])])
        visited.add(dst)
        tree.add((src,dst))
        nodes[dst] = nodes[src] + edges[(src,dst)]
    return tree

def get_dist(v1,v2):
    lat1, lon1 = v1
    lat2, lon2 = v2
    x = (lon2 - lon1) * math.cos((lat1+lat2)/2)
    y = (lat2 - lat1)
    return (x*x +y*y)**0.5 * 6371

start_point = input().split(':')[1]
end_point = input().split(':')[1]
n = int(input())
names = {}
locations = {}
nodes = {}
INFINITY = 6371 * math.pi

for i in range(n):
    k, name, na, lat, lon, na, na, na, na = input().split(',')
    k = k.split(':')[1]
    names[k] = name[1:-1]
    locations[k] = [math.radians(float(lat)),math.radians(float(lon))]
    nodes[k] = INFINITY

m = int(input())
edges = {}
for i in range(m):
    v1,v2 = input().split()
    v1 = v1.split(':')[1]
    v2 = v2.split(':')[1]
    if v1 != v2: edges[(v1,v2)] = get_dist(locations[v1],locations[v2])

find_shortest_paths(start_point,end_point,nodes,edges)

#for k in nodes: print("Node:", k,nodes[k], file=sys.stderr)
#for e in edges: print("Edge:", e, edges[e], file=sys.stderr)
#print(start_point, end_point, file=sys.stderr)

curr = end_point
if nodes[end_point] == INFINITY:
    print("IMPOSSIBLE")
else:
    path = [end_point]
    while True:
        if nodes[curr] == 0: break
        curr, na = min([(x,y) for x,y in edges if y == curr], key=lambda e: nodes[e[0]] + edges[(e[0],e[1])])
        path.insert(0,curr)

    print(path, file=sys.stderr)
    for n in path: print(names[n])
