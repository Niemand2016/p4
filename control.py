#!/usr/bin/python

import sys, struct

from scapy.all import sniff

sys.path.append('gen-py')

from bm_runtime.standard import Standard
from bm_runtime.standard.ttypes import *

from sswitch_runtime import SimpleSwitch
from sswitch_runtime.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TMultiplexedProtocol

def connect():
    transport = TSocket.TSocket('127.0.0.1', 9090)
    transport = TTransport.TBufferedTransport(transport)
    transport.open()
    return transport;

def get_clients(transport):
    bProtocol = TBinaryProtocol.TBinaryProtocol(transport)
    protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'standard')
    stdClient = Standard.Client(protocol)
    protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'simple_switch')
    ssClient = SimpleSwitch.Client(protocol)
    return (stdClient,ssClient)

def init_switch(stdClient, ssClient):
    stdClient.bm_mt_set_default_action(0,'forward','drop_packet',None)
    stdClient.bm_mt_set_default_action(0,'ipv4_flows','add_flow',None)
    stdClient.bm_mt_set_default_action(0,'ipv6_flows','add_flow',None)
    stdClient.bm_mt_set_default_action(0,'bloom_table','update_bloom',None)
    match_key = [BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact('\x00\x00'))]
    stdClient.bm_mt_add_entry(0, 'forward', match_key, 'set_egr', ['\x00\x01'], None)
    match_key = [BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact('\x00\x01'))]
    stdClient.bm_mt_add_entry(0, 'forward', match_key, 'set_egr', ['\x00\x00'], None)
    ssClient.mirroring_mapping_add(0,9)
    match_key = [BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact('\x08\x00'))]
    stdClient.bm_mt_add_entry(0, 'build', match_key, 'build_ipv4_flow', None, None)
    match_key = [BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact('\x86\xdd'))]
    stdClient.bm_mt_add_entry(0, 'build', match_key, 'build_ipv6_flow', None, None)

def add_ipv4_flow(pkt, stdClient, ssClient):
    src_ip = str(pkt)[2:6]
    dst_ip = str(pkt)[6:10]
    proto  = str(pkt)[10]
    sport  = str(pkt)[11:13]
    dport  = str(pkt)[13:15]
    timestamp = struct.unpack(">Q", '\x00\x00' + str(pkt)[15:21])[0]
    print(timestamp)
    match_key = []
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(src_ip)))
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(dst_ip)))
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(proto)))
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(sport)))
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(dport)))
    entryID = stdClient.bm_mt_add_entry(0, 'ipv4_flows', match_key, 'update_ipv4_flow', ['\x00\x00\x00\x00'], None)
    action_data = struct.pack(">I", entryID)
    stdClient.bm_mt_modify_entry(0, 'ipv4_flows', entryID, 'update_ipv4_flow', [action_data])
    #timestamp = ssClient.get_time_elapsed_us()
    stdClient.bm_register_write(0, 'ipv4_timestamp', entryID, timestamp)

def add_ipv6_flow(pkt, stdClient, ssClient):
    src_ip = str(pkt)[2:18]
    dst_ip = str(pkt)[18:34]
    proto  = str(pkt)[34]
    sport  = str(pkt)[35:37]
    dport  = str(pkt)[37:39]
    timestamp = struct.unpack(">Q", '\x00\x00' + str(pkt)[39:45])[0]
    match_key = []
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(src_ip)))
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(dst_ip)))
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(proto)))
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(sport)))
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(dport)))
    entryID = stdClient.bm_mt_add_entry(0, 'ipv6_flows', match_key, 'update_ipv6_flow', ['\x00\x00\x00\x00'], None)
    action_data = struct.pack(">I", entryID)
    stdClient.bm_mt_modify_entry(0, 'ipv6_flows', entryID, 'update_ipv6_flow', [action_data])
    #timestamp = ssClient.get_time_elapsed_us()
    stdClient.bm_register_write(0, 'ipv6_timestamp', entryID, timestamp)

def add_flow(pkt, stdClient, ssClient):
    ethertype = str(pkt)[0:2]
    if ethertype == '\x08\x00':
        add_ipv4_flow(pkt, stdClient, ssClient)
    elif ethertype == '\x86\xdd':
        add_ipv6_flow(pkt, stdClient, ssClient)
        
connection = connect()
(stdClient, ssClient) = get_clients(connection)
init_switch(stdClient, ssClient)

sniff(iface='veth9', prn=lambda x: add_flow(x, stdClient, ssClient))

connection.close()
