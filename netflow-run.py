#!/usr/bin/python

import sys, struct, socket

from scapy.all import sniff

sys.path.append('gen-py')

from bm_runtime.standard import Standard
from bm_runtime.standard.ttypes import *

from sswitch_runtime import SimpleSwitch
from sswitch_runtime.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TMultiplexedProtocol

def connect():
    transport = TSocket.TSocket('127.0.0.1', 9090)
    transport = TTransport.TBufferedTransport(transport)
    transport.open()
    return transport;

def get_client(transport):
    bProtocol = TBinaryProtocol.TBinaryProtocol(transport)
    protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'standard')
    stdClient = Standard.Client(protocol)
    #protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'simple_switch')
    #ssClient = SimpleSwitch.Client(protocol)
    return (stdClient)

def print_ipv4_flow(pkt):
    in_port   = struct.unpack_from('!H',pkt,0)[0]
    out_port  = struct.unpack_from('!H',pkt,2)[0]
    src_ip    = socket.inet_ntoa(pkt[4:8])
    dst_ip    = socket.inet_ntoa(pkt[8:12])
    proto     = struct.unpack_from('!B',pkt,12)[0]
    src_port  = struct.unpack_from('!H',pkt,13)[0]
    dst_port  = struct.unpack_from('!H',pkt,15)[0]
    timestamp = struct.unpack("!Q", '\x00\x00' + pkt[17:23])[0]

    print 'Flow: %s:%d -> %s:%d' % (src_ip, src_port, dst_ip, dst_port),
    print '[%d] (Inf%d -> Inf%d)' % (proto, in_port, out_port),
    print '%.2fs' % (timestamp/1000000.)

def appendKey(keys,para) :
        keys.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(para)))

def add_ipv4_flow(pkt, client):
    print_ipv4_flow(pkt)
    in_port   = pkt[0:2]
    out_port  = pkt[2:4]
    src_ip    = pkt[4:8]
    dst_ip    = pkt[8:12]
    proto     = pkt[12]
    src_port  = pkt[13:15]
    dst_port  = pkt[15:17]
    timestamp = struct.unpack("!Q", '\x00\x00' + pkt[17:23])[0]

    match_key = []
    appendKey(match_key, in_port);
    appendKey(match_key, out_port);
    appendKey(match_key, src_ip);
    appendKey(match_key, dst_ip);
    appendKey(match_key, proto);
    appendKey(match_key, src_port);
    appendKey(match_key, dst_port);
    entryID = client.bm_mt_add_entry(0, 'ipv4_flows', match_key, 'update_ipv4_flow', ['\x00\x00\x00\x00'], None)
    action_data = struct.pack(">I", entryID)
    client.bm_mt_modify_entry(0, 'ipv4_flows', entryID, 'update_ipv4_flow', [action_data])
    #stdClient.bm_register_write(0, 'ipv4_timestamp', entryID, timestamp)

connection = connect()
client = get_client(connection)

sniff(iface='veth9', prn=lambda x: add_ipv4_flow(str(x), client))

#sniff(iface='veth9', prn=lambda x: add_ipv4_flow(str(x), stdClient, ssClient))

connection.close()
