import sys
import math
import time 

s1 = time.process_time()

def printd(*args):
    print(*args, file=sys.stderr)

def printd_path(path):
    cost = sum([v.get_cost() for v in path])
    print("Path:", cost, [v.get_name() for v in path], file=sys.stderr)

class Vertex:
    def __init__(self, name):
        self.adjacencies = set()
        self.name = name
        self.cost = 0
        self.path_cost = 0
        
    def add_adjacency(self, vertex):
        self.adjacencies.add(vertex)
    
    def remove_adjacency(self, vertex):
        self.adjacencies.remove(vertex)
    
    def get_adjacencies(self):
        return self.adjacencies
        
    def get_degree(self):
        return len(self.adjacencies)
    
    def get_name(self):
        return self.name
        
    def get_cost(self):
        return self.cost
    
    def set_cost(self, cost):
        self.cost = cost
        
class Graph:
    def __init__(self):
        self.verticies = {}
        
    def add_vertex(self, name):
        self.verticies[name] = Vertex(name)
    
    def get_verticies(self):
        return [self.verticies[k] for k in self.verticies.keys()]
    
    def get_vertex(self, name):
        return self.verticies[name]
    
    def add_edge(self,name1,name2):
        self.verticies[name1].add_adjacency(self.verticies[name2])
            
    def remove_edge(self,name1,name2):
        self.verticies[name1].remove_adjacency(self.verticies[name2])
        
    def get_num_verticies(self):
        return len(self.verticies)

def walk(v,g,c):
    c += v.cost
    max_money = c
    if c > v.path_cost:
        v.path_cost = c
        for adj in v.adjacencies:
            money = walk(adj,g,c)
            if money > max_money: max_money = money
    return max_money

g = Graph()
E = set()
V = {}
num_rooms = int(input())
##printd(num_rooms)
for i in range(num_rooms):
    g.add_vertex(str(i))

g.add_vertex('E')

for i in range(num_rooms):
    v, cost, adj1, adj2 = input().split()
    if adj1.isnumeric() and int(adj1) <= int(v):
        printd(v,adj1)
    if adj2.isnumeric() and int(adj2) <= int(v):
        printd(v,adj2)
    g.get_vertex(v).set_cost(int(cost))

    g.add_edge(v,adj1)
    g.add_edge(v,adj2)

start = g.get_vertex('0')

s2 = time.process_time()
printd("Time:", s2-s1)

answer = walk(start,g,0)

#printd("Nodes:",V)
#printd("Edges:",E)
s2 = time.process_time()
printd("Time:", s2-s1)

print(answer)
