#!/usr/bin/python2

import sys

class Graph:
    def __init__(self):
        self.edges = set()
        self.verticies = set()
        self.segments = set()

    def add_vertex(self, name):
        v = Vertex(name)
        self.verticies.add(v)
        return v

    def get_vertex(self, name):
        for v in self.verticies:
            if v.id == name: return v
        v = self.add_vertex(name)
        return v

    def add_edge(self, name1, name2, edge_id, weight):
        v1 = self.get_vertex(name1)
        v2 = self.get_vertex(name2)
        self.edges.add(Edge(v1, v2, edge_id, weight))
        self.edges.add(Edge(v2, v1, edge_id, weight))

class Edge:
    def __init__(self, v1, v2, id, weight):
        self.id = id
        self.v1 = v1
        self.v2 = v2
        self.weight = weight

class Vertex:
    def __init__(self, name):
        self.id = name

def next_hops(g,v):
    visited = {}
    next_hop = {}
    visited[v] = 0
    next_hop[v] = None

    while 1:
        canidate_edges = [e for e in g.edges if e.v1 in visited and e.v2 not in visited]
        if len(canidate_edges) == 0: break
        e = min(canidate_edges, key=lambda e: visited[e.v1] + e.weight)
        visited[e.v2] = visited[e.v1] + e.weight
        if next_hop[e.v1] == None:
            next_hop[e.v2] = e
        else:
            next_hop[e.v2] = next_hop[e.v1]

    return next_hop

def parse_input():
    f = open(sys.argv[1], 'r')
    g = Graph()
    seg = 1
    for line in f:
        v1, v2, w = [int(j) for j in line.split()]
        g.add_edge(v1, v2, seg, w)
        #g.add_edge(v2, v1, seg, w)
        seg += 1
    return g

def gen_vrf_table(g):
    for e in g.edges:
            # (RX dest) MAC Address => set_vrf() (This) VRF, (Logical) Ingress Interface !!TODO
            print "table_add vrf_table set_vrf 02:00:%02x:00:00:%02x => %d" % (e.id, e.v1.id, e.v1.id)

def gen_routing_tabe(g):
    for v in g.verticies:
        nh = next_hops(g,v)
        inf = 0
        for dst in nh:
            if nh[dst] == None:
                # (This) VRF, (Dest VRF) IP => send_to_cpu()
                print "table_add ipv4_routing_table send_to_cpu %d 10.0.%d.0/24 =>" % (v.id, dst.id)
                continue
            else:
                phy_port = int(nh[dst].v1.id < nh[dst].v2.id) + 1
                # (This) VRF, (Dest VRF) IP => set_next_hop() (Next Hop) VRF, (Logical) Egress Interface, (Physical) Egress Interface
                print "table_add ipv4_routing_table set_next_hop %d 10.0.%d.0/24 => %d %d %d" % (v.id, dst.id, nh[dst].v2.id, inf, phy_port)
                # (This) VRF, (Logical) Egress Interface => set_smac() (TX source) MAC Address
                print "table_add smac_table set_smac %d %d => 02:00:%02x:00:00:%02x" % (v.id, inf, nh[dst].id, nh[dst].v1.id)
                inf += 1

def gen_dmac_table(g):
        for e in g.edges:
                # (This) VRF, (Next Hop) VRF => set_dmac() (TX dest) MAC Address
                print "table_add dmac_table set_dmac %d %d => 02:00:%02x:00:00:%02x" % (e.v1.id, e.v2.id, e.id, e.v2.id)

# TODO: Set Default Actions
g = parse_input()
gen_vrf_table(g)
gen_routing_tabe(g)
gen_dmac_table(g)
