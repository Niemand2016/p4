#define STANDARD_PARSER_RETURN ip_netflow
//define STANDARD_PARSER_RETURN ingress
#define NETFLOW_PARSER_RETURN ingress
#define NETFLOW_CONTROL_RETURN dlp
#define LOGICAL_IN_PORT local_metadata.rx_log_port
#define LOGICAL_OUT_PORT local_metadata.tx_log_port
#define LOGICAL_PORT_SIZE 8

#include "nfp-metadata.p4"
#include "standard_parser.p4"
#include "netflow.p4"

parser start {
	set_metadata(standard_metadata.egress_spec, standard_metadata.clone_spec);
	return standard_parser;
}

// Headers

header_type local_metadata_t {
	fields {
		vrf         : 8;
        rx_log_port : 8;
        tx_log_port : 8;
		cpu_code    : 8;
	}
}
metadata local_metadata_t local_metadata;

// Field List

field_list clone_fields {
	LOGICAL_IN_PORT;
	LOGICAL_OUT_PORT;
	TIMESTAMP_FIELD;
	local_metadata.cpu_code;
}

// Actions

action drop_packet() {
	drop();
}

action set_ingress(vrf, rx_log_port) {
	modify_field(local_metadata.vrf, vrf);
    modify_field(local_metadata.rx_log_port, rx_log_port);
}

action set_next_hop(tx_log_port) {
    modify_field(local_metadata.tx_log_port, tx_log_port);
}

action set_egress(smac, dmac, tx_phy_port) {
    modify_field(ether_hdr.src, smac);
    modify_field(ether_hdr.dst, dmac);
    modify_field(standard_metadata.egress_spec, tx_phy_port);
}

action clone_to_cpu(cpu_port, reason) {
	clone_ingress_pkt_to_ingress(cpu_port, clone_fields);
	modify_field(local_metadata.cpu_code, reason);
}

action send_to_cpu(cpu_port, reason) {
	modify_field(standard_metadata.egress_spec, cpu_port);
	modify_field(local_metadata.cpu_code, reason);
}

action add_cpu_hdr() {
	add_header(cpu_hdr);
	modify_field(cpu_hdr.code, local_metadata.cpu_code);
	modify_field(cpu_hdr.ethertype, ether_hdr.ethertype);
	modify_field(ether_hdr.ethertype, 0x88b5);
	modify_field(cpu_hdr.in_port, LOGICAL_IN_PORT);
	modify_field(cpu_hdr.out_port, LOGICAL_OUT_PORT);
	modify_field(cpu_hdr.timestamp, TIMESTAMP_FIELD);
}

action no_operation() {
	no_op();
}

// Tables

table ingress_table {
	reads {
		ether_hdr.dst : exact;
	}
	actions {
		set_ingress;
		drop_packet;
	}
}

table ipv4_routing_table {
	reads {
		local_metadata.vrf : exact;
		ipv4_hdr.dst       : lpm;
	}
	actions {
		set_next_hop;
		drop_packet;
	}
}

table egress_table {
    reads {
        local_metadata.vrf         : exact;
        local_metadata.tx_log_port : exact;
    }
	actions {
        set_egress;
        drop_packet;
	}
}

// Determines which ports goto the control plane and therforee recieve a
// control plane header.
table cpu_hdr_table {
	reads {
		standard_metadata.egress_port : exact;
	}
	actions {
		add_cpu_hdr;
		no_operation;
	}
}

// Control

control ingress {
	if (standard_metadata.clone_spec == 0) {
		apply(ingress_table);
		if (valid(ipv4_hdr)) { apply(ipv4_routing_table); }
	    apply(egress_table);
	    netflow();
	}
}

control egress {
		apply(cpu_hdr_table);
}
