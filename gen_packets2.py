#!/usr/bin/python2

from scapy.all import *
import random, os

NETWORKS = 4
FLOWS = 1
PACKETS = 2
INTERFACE = 'veth1'
DEBUG_PRINT_PKTS = True

def gen_networks(nets):
    return ['10.0.%d.0/24' % (x) for x in range(1, nets+1)]

def gen_flows(count, nets):
    s = set()
    while (len(s) < count):
        snet = random.choice(nets)
        dnet = random.choice(nets)
        if (snet == dnet): continue
        src = str(RandIP(snet))
        dst = str(RandIP(dnet))
        proto = random.choice([6,17])
        s.add((proto, src, int(RandShort()), dst, int(RandShort())))
    return s

def parse_vrf(net):
    return int(net.split('.')[2])

def sendpkts(count, flows):
    pkt_frmt = "#%d (%d), %s:%d (%s) -> (%s) %s:%d"
    for i in range(count):
        (proto, src, sport, dst, dport) = random.sample(flows, 1)[0]
        vrf = parse_vrf(src)
        smac = '02:00:00:00:00:00'
        dmac = "02:00:00:00:00:%02x" % (vrf)
        pkt = Ether(src=smac,dst=dmac)/IP(src=src,dst=dst)
        if (proto == 6): pkt = pkt/TCP(sport=sport,dport=dport)
        elif (proto == 17): pkt = pkt/UDP(sport=sport,dport=dport)
        else: print('ERROR: Unsupported Protocol')
        payload = os.urandom(random.randint(64,256))
        pkt.add_payload(payload)
        if (i % 10000 == 0): print(i)
        if (DEBUG_PRINT_PKTS):
            print(pkt_frmt % (i, proto, src, sport, smac, dmac, dst, dport))
        sendp(pkt, iface=INTERFACE)


nets = gen_networks(NETWORKS)
flows = gen_flows(FLOWS, nets)
sendpkts(PACKETS, flows)
