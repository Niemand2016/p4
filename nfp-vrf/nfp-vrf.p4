#define STANDARD_PARSER_RETURN ingress
#define LOGICAL_PORT_SIZE 8

#include "../common/standard_parser.p4"
#include "../common/standard_actions.p4"

parser start {
    return standard_parser;
}

// Headers

header_type vrf_metadata_t {
    fields {
        id      : 8;
        rx_port : 8;
        tx_port : 8;
    }
}
metadata vrf_metadata_t vrf_metadata;

// Actions

action set_ingress(vrf_id, vrf_rx_port) {
    modify_field(vrf_metadata.id, vrf_id);
    modify_field(vrf_metadata.rx_port, vrf_rx_port);
}

action set_next_hop(vrf_tx_port) {
    modify_field(vrf_metadata.tx_port, vrf_tx_port);
}

action set_egress(smac, dmac, egress_port) {
    modify_field(ether_hdr.src, smac);
    modify_field(ether_hdr.dst, dmac);
    modify_field(standard_metadata.egress_spec, egress_port);
}

// Tables

table ingress_table {
    reads {
        ether_hdr.dst : exact;
    }
    actions {
        set_ingress;
        drop_packet;
    }
}

table ipv4_routing_table {
    reads {
        vrf_metadata.id : exact;
        ipv4_hdr.dst    : lpm;
    }
    actions {
        set_next_hop;
        drop_packet;
    }
}

table egress_table {
    reads {
        vrf_metadata.id      : exact;
        vrf_metadata.tx_port : exact;
    }
    actions {
        set_egress;
        drop_packet;
    }
}

// Determines which ports goto the control plane and therforee recieve a
// control plane header.


// Control

control ingress {
    apply(ingress_table);
    if (valid(ipv4_hdr)) { apply(ipv4_routing_table); }
    apply(egress_table);
}

control egress {
}
