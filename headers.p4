// Ethernet II
// 4 Byte Trailer (CRC Checksum) Not Included
header_type ether_hdr_t {
	fields {
		dst       : 48;
		src       : 48;
		ethertype : 16;
	}
}

// 802.1Q VLAN Tag
// EtherType: 0x8100
header_type vlan_hdr_t {
    fields {
        pcp       : 3;
        dei       : 1;
        vid       : 12;
        ethertype : 16;
    }
}

// IPv4 - RFC 791
// EtherType: 0x0800
// Protocol Number: 4
header_type ipv4_hdr_t {
	fields {
		version   : 4;
		ihl       : 4;
		tos       : 8;
		total_len : 16;
		id        : 16;
		flags     : 3;
		offset    : 13;
		ttl       : 8;
		proto     : 8;
		checksum  : 16;
		src       : 32;
		dst       : 32;
		options   : *;
	}
	length     : 4 * ihl;
	max_length : 60; // Min 20
}

// IPv6 - RFC 2460
// EtherType: 0x86dd
// Protocol Number: 41
header_type ipv6_hdr_t {
	fields {
		version       : 4;
		traffic_class : 8;
		flow_label    : 20;
		payload_len   : 16;
		next_hdr      : 8;
		hop_limit     : 8;
		src           : 128;
		dst           : 128;
	}
}

// TCP - RFC 793
// Protocol Number: 6
header_type tcp_hdr_t {
	fields {
		src      : 16;
		dst      : 16;
		seq_num  : 32;
		ack_num  : 32;
		offset   : 4;
		reserved : 6;
		flags    : 6;
		window   : 16;
		checksum : 16;
		urg_ptr  : 16;
		options  : *;
	}
	length     : 4 * offset;
	max_length : 60; // Min 20
}

// UDP - RFC 768
// Protocol Number: 17
header_type udp_hdr_t {
	fields {
		src      : 16;
		dst      : 16;
		len      : 16;
		checksum : 16;
	}
}
