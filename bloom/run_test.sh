#!/bin/bash

PKTS=$1
RUNS=$2

./capture.py $PKTS $RUNS &

for ((i=1;i<=$RUNS;i+=1))
do
    echo "Sending $i Starting"
    echo "register_reset bloom_filter" | simple_switch_CLI --json config.json > /dev/null
    sleep 1
    ./send.py $PKTS > /dev/null
    echo "Sending $i Complete"
done
echo "Done Sending"
