#include "flow_parser.p4"
#include "standard_actions.p4"

#define BF_PADDING 7
#define BF_SALT1 0x7742
#define BF_SALT2 0x2eb7
#define BF_SALT3 0xf3b4
#define BF_SALT4 0xe670
#define BF_WIDTH 408632

header_type bf_metadata_t {
    fields {
        bit_set : 1;
        in_set  : 1;
        index   : 64;
        salt    : 16;
        padding : BF_PADDING;
    }
}
metadata bf_metadata_t bf_metadata;

register bloom_filter {
    width : 1;
    instance_count : BF_WIDTH;
}

field_list bf_flow_fields {
    standard_metadata.ingress_port;
    ipv4_hdr.src;
    ipv4_hdr.dst;
    ipv4_hdr.proto;
    flow_metadata.src_port;
    flow_metadata.dst_port;
    bf_metadata.salt;
    bf_metadata.padding;
}

field_list_calculation bf_hash_func {
    input {
        bf_flow_fields;
    }
    algorithm : xxh64;
    output_width : 64;
}

action bf_query() {
    modify_field(bf_metadata.in_set, 1);

    bf_check_bit(BF_SALT1);
    bf_check_bit(BF_SALT2);
    bf_check_bit(BF_SALT3);
    bf_check_bit(BF_SALT4);
    //bf_check_bit(BF_SALT5);
}

action bf_check_bit(salt) {
    modify_field(bf_metadata.salt, salt);
    modify_field_with_hash_based_offset(bf_metadata.index, 0, bf_hash_func, BF_WIDTH);
    register_read(bf_metadata.bit_set, bloom_filter, bf_metadata.index);
    bit_and(bf_metadata.in_set, bf_metadata.in_set, bf_metadata.bit_set);
}

action bf_insert() {
    bf_set_bit(BF_SALT1);
    bf_set_bit(BF_SALT2);
    bf_set_bit(BF_SALT3);
    bf_set_bit(BF_SALT4);
    //bf_set_bit(BF_SALT5);

    modify_field(ipv4_hdr.tos, 1);
}

action bf_set_bit(salt) {
    modify_field(bf_metadata.salt, salt);
    modify_field_with_hash_based_offset(bf_metadata.index, 0, bf_hash_func, BF_WIDTH);
    register_write(bloom_filter, bf_metadata.index, 1);
}

table bf_check_table {
    reads {
        ether_hdr.ethertype : exact;
    }
    actions {
        drop_packet;
        bf_query;
    }
}

table bf_update_table {
    reads {
        ether_hdr.ethertype : exact;
    }
    actions {
        drop_packet;
        bf_insert;
    }
}

control ingress {
    apply(bf_check_table);
    if (bf_metadata.in_set == 0) {
        apply(bf_update_table);
    }
}

control egress {
}
