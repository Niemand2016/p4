#!/usr/bin/python2

from scapy.all import *
import random, os

PACKETS = int(sys.argv[1])
INTERFACE = 'veth1'
DEBUG_PRINT_PKTS = False

def gen_pkts():
    s = set()
    while (len(s) < PACKETS):
        src = str(RandIP())
        dst = str(RandIP())
        proto = random.choice([6,17])
        sport = int(RandShort());
        dport = int(RandShort());
        t = (proto, src, sport, dst, dport)
        if t not in s:
            s.add(t)
            sendpkt(t)

def sendpkt(t):
        (proto, src, sport, dst, dport) = t
        pkt = Ether(src=RandMAC(),dst=RandMAC())/IP(src=src,dst=dst)
        if (proto == 6): pkt = pkt/TCP(sport=sport,dport=dport)
        elif (proto == 17): pkt = pkt/UDP(sport=sport,dport=dport)
        payload = os.urandom(64)
        pkt.add_payload(payload)
        if (DEBUG_PRINT_PKTS):
            print(t)
        sendp(pkt, iface=INTERFACE)

gen_pkts()
