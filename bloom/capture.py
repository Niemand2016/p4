#!/usr/bin/python2

from scapy.all import *
import random, os, sys

PACKETS = int(sys.argv[1])
RUNS = int(sys.argv[2])
INTERFACE = 'veth0'
count = 0
hit = [0]*PACKETS
miss = [0]*PACKETS

def recvpkt(p):
    global count
    if (p.payload.tos == 1):
        miss[count] += 1
    else:
        hit[count] += 1
    count += 1

for i in range(RUNS):
    print("Capture %d Starting" % (i+1))
    sniff(iface=INTERFACE, count=PACKETS, prn=recvpkt)

    fh = open('stats.csv', 'w')
    fh.write("Entries, Hits, Misses\n")
    for j in range(PACKETS):
        total = hit[j] + miss[j]
        fh.write("%d, %d, %d\n" % (j, hit[j], miss[j]))
    fh.close()

    count = 0
    print("Capture %d Complete" % (i+1))
