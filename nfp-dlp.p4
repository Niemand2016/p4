// REQUIREMENTS
// Parser: standard_parser.p4
// Control Function: NETFLOW_PARSER_RETURN
// Metadata Fields: LOGICAL_IN_PORT

header_type dlp_hdr_t {
	fields {
		node_id   : 16;
		label     : 16;
        ethertype : 16;
	}
}
header dlp_hdr_t dlp_hdr;

parser dlp_parser {
	extract(dlp_hdr);
	return select(dlp_hdr.ethertype) {
		0x0800  : ipv4;
		0x86dd  : ipv6;
//		0x88b5  : Experimental 1
//		0x88b6  : Experimental 2
		default : parse_error pe_unsupported_protocol;
	}
}

action set_dlp_label(label) {
    modify_field(dlp_metadata.label, label);
}

action send_to_cpu() {

}

table dlp_table {
    reads {
        dlp_hdr.node_id : exact;
        dlp_hdr.label   : exact;
        LOGICAL_IN_PORT : exact;
    }
    actions {
        set_dlp_label;
        send_to_cpu;
    }
}

control dlp {
    apply(dlp_table);
}
