// Headers

header_type ipv4_flow_t {
	fields { // Length: 184 bits (23 Bytes)
		in_port   : 16;
		out_port  : 16;
        src_ip    : 32;
        dst_ip    : 32;
        proto     : 8;
		src_port  : 16;
		dst_port  : 16;
		timestamp : 64;
	}
}

header ipv4_flow_t ipv4_flow;

// Field List

// Parser

parser ip_netflow {
	return select(ether_hdr.ethertype) {
		0x0800:	ipv4_netflow;
		//0x86dd: ipv6_netflow;
		default: ingress;
	}
}

parser ipv4_netflow {
	set_metadata(ipv4_flow.timestamp, intrinsic_metadata.ingress_global_timestamp);
	set_metadata(ipv4_flow.src_ip, ipv4_hdr.src);
	set_metadata(ipv4_flow.dst_ip, ipv4_hdr.dst);
	set_metadata(ipv4_flow.proto, ipv4_hdr.proto);
	return select(ipv4_hdr.proto) {
		0x06    : tcp_netflow;
		0x11    : udp_netflow;
//		0x84    : sctp;
		default: ingress;
	}
}

parser tcp_netflow {
	set_metadata(flow_metadata.src_port, tcp_hdr.src);
	set_metadata(flow_metadata.dst_port, tcp_hdr.dst);
	return ingress;
}

parser udp_netflow {
	set_metadata(flow_metadata.src_port, udp_hdr.src);
	set_metadata(flow_metadata.dst_port, udp_hdr.dst);
	return ingress;
}

// Actions

action update_ipv4_flow() {
//	Update Flow Last Seen Timestamp
}

action add_flow() {
//	Send Copy to CPU to Create Flow Entry
}

// Tables

table ipv4_flow_table {
	reads {
		local_metadata.rx_log_port : exact;
		local_metadata.tx_log_port : exact;
        ipv4_flow.src_ip    : exact;
        ipv4_flow.dst_ip    : exact;
        ipv4_flow.proto     : exact;
		ipv4_flow.src_port  : exact;
		ipv4_flow.dst_port  : exact;
		ipv4_flow.timestamp : exact;
	}
	actions {
		update_ipv4_flow;
		add_flow;
	}
}

// Control

control netflow {
	if (valid(ipv4_hdr)) { apply(ipv4_flow_table); }
//	if (valid(ipv6_hdr)) { apply(ipv6_flow_table); }
}
