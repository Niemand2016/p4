#!/usr/bin/python3

import sys, json, subprocess

CPU_CODE_NETFLOW = 0x1
CLOCK_FREQ = 600000000

ipv4_flow_counter = 0
ipv6_flow_counter = 0

class Packet:
    def __init__(self, data):
        offset = 0
        self.vrf       = None
        self.dst_mac   = None
        self.src_mac   = None
        self.ethertype = None
        self.cpu_code  = None
        self.rx_port   = None
        self.tx_port   = None
        self.timestamp = None
        self.proto     = None
        self.src_ip    = None
        self.dst_ip    = None
        self.src_port  = None
        self.dst_port  = None

        offset += self.__parse_ethernet(data, offset)

        if self.ethertype == 0x88b5:
            offset += self.__parse_cpu(data, offset)
        if self.ethertype == 0x0800:
            offset += self.__parse_ipv4(data, offset)
        elif self.ethertype == 0x86dd:
            offset += self.__parse_ipv6(data, offset)
        else:
            sys.exit('Unsupported Protocol')

        if self.proto in (6, 17, 132):
            offset += self.__parse_l4(data, offset)

    def __parse_ethernet(self, pkt_data, offset):
        data = pkt_data[offset:]
        self.vrf       = data[5]
        self.dst_mac   = int.from_bytes(data[0:6], 'big')
        self.src_mac   = int.from_bytes(data[6:12], 'big')
        self.ethertype = int.from_bytes(data[12:14], 'big')
        return 14 # Length of Ethernet header

    def __parse_cpu(self, pkt_data, offset):
        data = pkt_data[offset:]
        self.cpu_code  = data[0]
        self.ethertype = int.from_bytes(data[1:3], 'big')
        self.rx_port   = data[3]
        self.tx_port   = data[4]
        self.timestamp = int.from_bytes(data[5:13], 'big')
        return 13 # Length of CPU header

    def __parse_ipv4(self, pkt_data, offset):
        data = pkt_data[offset:]
        hdr_len = data[0] & 15
        self.proto   = data[9]
        self.src_ip  = int.from_bytes(data[12:16], 'big')
        self.dst_ip  = int.from_bytes(data[16:20], 'big')
        return hdr_len * 4 # Length of IPv4 header including options

    def __parse_ipv6(self, pkt_data, offset):
        data = pkt_data[offset:]
        self.proto   = data[6]
        self.src_ip  = int.from_bytes(data[8:24], 'big')
        self.dst_ip  = int.from_bytes(data[24:40], 'big')
        return 40 # Length of IPv6 header

    def __parse_l4(self, pkt_data, offset):
        data = pkt_data[offset:]
        self.src_port = int.from_bytes(data[0:2], 'big')
        self.dst_port = int.from_bytes(data[2:4], 'big')
        return 4 # Amount of data parsed, not used

def ipv4_to_str(ip):
    octets = ip.to_bytes(4, 'big')
    return '.'.join(["%d" % octet for octet in octets])

def ipv6_to_str(ip):
    bDubs = [ip[i:i+2] for i in range(0,16,2)]
    sDubs = ["%04x" % int.from_bytes(dub, 'big') for dub in bDubs]
    return ':'.join(sDubs)

def mac_to_str(mac):
    byts = mac.to_bytes(6, 'big')
    return ':'.join(["%02x" % byte for byte in byts])

def read_bytes(length):
    byts = sys.stdin.buffer.raw.read(length)
    if len(byts) == 0:
        sys.exit('EOF')
    if len(byts) != length:
        sys.exit('Read returned to few bytes')
    return byts

def read_field():
    field = sys.stdin.buffer.raw.read(4)
    if len(field) == 0:
        sys.exit('EOF')
    if len(field) != 4:
        sys.exit('Read returned to few bytes')
    return int.from_bytes(field, 'little')

def decode_packet_block(block_length):
    read_bytes(12) # Discard Some Metadata
    capture_len = read_field()
    read_bytes(4) # Discard Some Metadata
    data = read_bytes(capture_len)
    pkt = Packet(data)
    read_bytes(block_length - 28 - capture_len) # Discard remainder of block
    return pkt

def print_packet(pkt):
    if pkt.cpu_code != None:
        timestamp = pkt.timestamp * 16 / CLOCK_FREQ
        print("CPU Code: %d, Timestamp: %0.3f" % (pkt.cpu_code, timestamp))
        print("VRF: %d, Inf: %d -> %d" % (pkt.vrf, pkt.rx_port, pkt.tx_port))

    src_mac = mac_to_str(pkt.src_mac)
    dst_mac = mac_to_str(pkt.dst_mac)
    print("Ethr: %s -> %s (%#06x)" % (src_mac, dst_mac, pkt.ethertype))

    if pkt.ethertype == 0x0800:
        src_ip = ipv4_to_str(pkt.src_ip)
        dst_ip = ipv4_to_str(pkt.dst_ip)
    elif pkt.ethertype == 0x86dd:
        src_ip = ipv6_to_str(pkt.src_ip)
        dst_ip = ipv6_to_str(pkt.dst_ip)

    if pkt.proto != None:
        print("IPv4: %s -> %s (Proto: %d)" % (src_ip, dst_ip, pkt.proto))

    if pkt.src_port != None and pkt.dst_port != None:
        print("Lyr4: %d -> %d" % (pkt.src_port, pkt.dst_port))

    print("")

def add_ipv4_flow(pkt):
    global ipv4_flow_counter
    ipv4_flow_counter += 1
    match = {}
    match['local_metadata.vrf']         = {'value': pkt.vrf}
    match['local_metadata.rx_log_port'] = {'value': pkt.rx_port}
    match['local_metadata.tx_log_port'] = {'value': pkt.tx_port}
    match['ipv4_hdr.src']               = {'value': pkt.src_ip}
    match['ipv4_hdr.dst']               = {'value': pkt.dst_ip}
    match['ipv4_hdr.proto']             = {'value': pkt.proto}
    match['flow_metadata.src_port']     = {'value': pkt.src_port}
    match['flow_metadata.dst_port']     = {'value': pkt.dst_port}
    action = {'type': 'update_ipv4_flow'}

    args = ['./rtecli']
    args.extend("-r nitrogen.sdn.surfnet.nl -p 20206".split(' '))
    args.extend("tables -t ipv4_flow_table".split(' '))
    args.extend(["-r", str(ipv4_flow_counter)])
    args.extend(["-m", json.dumps(match)])
    args.extend(["-a", json.dumps(action)])
    args.append("add")
    subprocess.run(args)
    print(args)

def read_block():
    block_type = read_field()
    block_length = read_field()

    if block_type != None:
        #print("Block Type: %#010x" % block_type)
        #print("Block Length: %#010x (%d)" % (block_length, block_length))

        if block_type == 6:
            pkt = decode_packet_block(block_length)
            if pkt.cpu_code == CPU_CODE_NETFLOW:
                if pkt.ethertype == 0x0800:
                    add_ipv4_flow(pkt)
            print_packet(pkt)
        else:
            read_bytes(block_length - 8)
    return block_type

while read_block() != None:
    pass
