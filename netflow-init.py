#!/usr/bin/python

import sys, struct

sys.path.append('gen-py')

from bm_runtime.standard import Standard
from bm_runtime.standard.ttypes import *

from sswitch_runtime import SimpleSwitch
from sswitch_runtime.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TMultiplexedProtocol

def connect():
    transport = TSocket.TSocket('127.0.0.1', 9090)
    transport = TTransport.TBufferedTransport(transport)
    transport.open()
    return transport;

def get_clients(transport):
    bProtocol = TBinaryProtocol.TBinaryProtocol(transport)
    protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'standard')
    stdClient = Standard.Client(protocol)
    protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'simple_switch')
    ssClient = SimpleSwitch.Client(protocol)
    return (stdClient,ssClient)

def init_switch(stdClient, ssClient):
    stdClient.bm_mt_set_default_action(0,'forward','drop_packet',None)
    stdClient.bm_mt_set_default_action(0,'ipv4_flows','add_flow',None)
    match_key = [BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact('\x00\x00'))]
    stdClient.bm_mt_add_entry(0, 'forward', match_key, 'set_egr', ['\x00\x01'], None)
    match_key = [BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact('\x00\x01'))]
    stdClient.bm_mt_add_entry(0, 'forward', match_key, 'set_egr', ['\x00\x00'], None)
    ssClient.mirroring_mapping_add(0,9)
    match_key = [BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact('\x00\x09'))]
    stdClient.bm_mt_add_entry(0, 'build', match_key, 'build_ipv4_flow', None, None)
        
connection = connect()
(stdClient, ssClient) = get_clients(connection)
init_switch(stdClient, ssClient)

connection.close()
