#!/usr/bin/python2

import sys

CPU_PORT = 9
OUTPUT_FORMAT = "VRF: %(src)d, Dest: 10.0.%(dst)d.0/24 => 02:00:%(seg)02x:00:00:%(src)02x => 02:00:%(seg)02x:00:00:%(nxt)02x"

class Graph:
    def __init__(self):
        self.edges = set()
        self.verticies = set()
        self.segments = set()

    def add_vertex(self, name):
        v = Vertex(name)
        self.verticies.add(v)
        return v

    def add_segment(self, seg_id):
        seg = Segment(seg_id)
        self.segments.add(seg)
        return seg

    def get_vertex(self, name):
        for v in self.verticies:
            if v.id == name: return v
        v = self.add_vertex(name)
        return v

    def get_segment(self, seg_id):
        for seg in self.segments:
            if seg.id == seg_id: return seg
        seg = self.add_segment(seg_id)
        return seg

    def add_edge(self, name1, name2, seg_id, weight):
        v1 = self.get_vertex(name1)
        v2 = self.get_vertex(name2)
        seg = self.get_segment(seg_id)

        v1.add_interface(seg, weight)
        v2.add_interface(seg, weight)
        seg.add_adj(v1)
        seg.add_adj(v2)
        self.edges.add(Edge(v1, v2, seg, weight))
        self.edges.add(Edge(v2, v1, seg, weight))

class Edge:
    def __init__(self, v1, v2, seg, weight):
        self.seg = seg
        self.v1 = v1
        self.v2 = v2
        self.weight = weight

class Vertex:
    def __init__(self, name):
        self.id = name
        self.interfaces = []
        self.adjacencies = set()

    def get_interface(self, seg):
        if seg not in self.interfaces:
            self.interfaces.append(seg))
        return self.interfaces.index(seg)

    def add_adjacency(self, v, seg):
        inf = self.get_interface(seg)
        self.adjacencies.add(Adjacency(v, inf))

class Adjacency:
    def __init__(self, v, inf, cost):
        self.v = v
        self.inf = inf
        self.cost = cost

class Segment:
    def __init__(self, name):
        self.id = name
        self.adjacencies = set()

    def add_adj(self, v):
        self.adjacencies.add(v)

def next_hops(g,v):
    visited = {}
    next_hop = {}
    #tree = set()
    visited[v] = 0
    next_hop[v] = (0,0)

    while 1:
        canidate_edges = [e for e in g.edges if e.v1 in visited and e.v2 not in visited]
        if len(canidate_edges) == 0: break
        e = min(canidate_edges, key=lambda e: visited[e.v1] + e.weight)
        visited[e.v2] = visited[e.v1] + e.weight
        if next_hop[e.v1] == (0,0):
            next_hop[e.v2] = (e.seg.id, e.v2.id)
        else:
            next_hop[e.v2] = next_hop[e.v1]
        #tree.add(min_e)

    return next_hop

def parse_input():
    f = open(sys.argv[1], 'r')
    g = Graph()
    seg = 1
    for line in f:
        v1, v2, w = [int(j) for j in line.split()]
        g.add_edge(v1, v2, seg, w)
        #g.add_edge(v2, v1, seg, w)
        seg += 1
    return g

def gen_vrf_table(g):
    for v in g.verticies:
        for i in range(len(v.interfaces)):
            print "Segment: %d, VRF: %d => Interface: %d" % (v.interfaces[i].seg.id, v.id, i)
            #print "172.16.%d.%d" % (v.interfaces[i].seg, v.id)

def gen_routing_tabe(g):
    for v in g.verticies:
        nh = next_hops(g,v)
        for key in nh.keys():
            print OUTPUT_FORMAT % {'src': v.id, 'dst': key.id, 'seg': nh[key][0], 'nxt': nh[key][1]}

def gen_mac_table(g):
        for v in g.verticies:
            for i in range(len(v.interfaces)):
                print "172.16.%d.%d -> 02:00:%02x:00:00:%02x%%%d" % (v.interfaces[i].seg.id, v.id, v.interfaces[i].seg.id, v.id, i)

g = parse_input()
#gen_vrf_table(g)
#gen_mac_table(g)
gen_routing_tabe(g)

# (RX dest) MAC Address => (This) VRF
# (This) VRF, (Dest VRF) IP => (Next Hop) VRF, (This) VRF, (Local) Interface
# (This) VRF, (Local) Interface => (TX source) MAC Address
# (This) VRF, (Next Hop) VRF => (TX dest) MAC Address
