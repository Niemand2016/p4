// Headers

include "headers.p4"

header ether_hdr_t ether_hdr;

// Parser

parser start {
	extract(ether_hdr);
	return ingress;
}

// Control Functions

control ingress {
 	apply(mac_address_table);
}

control egress {
}

// Tables

table mac_address_table {
	reads {
		ether_hdr.dst : exact;
	}
	actions {
		set_egress;
		flood; // Default Action (Set by CP)
	}
}

// Actions

action set_egress(port) {
	modify_field(standard_metadata.egress_spec, port);
}

action flood() {
    // Packet replication is device dependent
    // modify_field(standard_metadata.egress_spec, FLOOD_SPEC);
}
