// Registers

register timestamp {
    width : 48;
    direct: flow;
}

// Headers

#include "headers.p4"

header_type intrinsic_metadata_t {
    fields {
        ingress_global_timestamp : 48;
        lf_field_list : 8;
        mcast_grp : 16;
        egress_rid : 16;
        resubmit_flag : 8;
        recirculate_flag : 8;
    }
}

header   ether_hdr_t          ether_hdr;
metadata intrinsic_metadata_t intrinsic_metadata;

// Parser

parser start {
	extract(ether_hdr);
	return ingress;
}

// Actions

action add_flow() {
	modify_field(standard_metadata.egress_spec, 1);
}

action update_flow() {
    register_write(timestamp, 0, intrinsic_metadata.ingress_global_timestamp);
    drop();
}

// Tables

table flow {
	reads {
		ether_hdr.src : exact;
		ether_hdr.dst : exact;
	}
	actions {
		update_flow;
		add_flow;
	}
}

// Control

control ingress {
 	apply(flow);
}

control egress {
}
