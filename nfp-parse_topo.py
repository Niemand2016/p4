#!/usr/bin/python2

import sys, json

CPU_PORT         = 'v0.1'
CPU_CODE_NETFLOW = 0x1
CPU_CODE_DLP     = 0x2

class Graph:
    def __init__(self):
        self.edges = set()
        self.verticies = set()

    def add_vertex(self, name):
        v = Vertex(name)
        self.verticies.add(v)
        return v

    def get_vertex(self, name):
        for v in self.verticies:
            if v.id == name: return v
        v = self.add_vertex(name)
        return v

    def add_edge(self, name1, name2, edge_id, weight):
        v1 = self.get_vertex(name1)
        v2 = self.get_vertex(name2)
        self.edges.add(Edge(v1, v2, edge_id, weight))
        self.edges.add(Edge(v2, v1, edge_id, weight))

class Edge:
    def __init__(self, v1, v2, id, weight):
        self.id = id
        self.v1 = v1
        self.v2 = v2
        self.weight = weight

class Vertex:
    def __init__(self, name):
        self.id = name

def next_hops(g,v):
    visited = {}
    next_hop = {}
    visited[v] = 0
    next_hop[v] = None

    while 1:
        canidate_edges = [e for e in g.edges if e.v1 in visited and e.v2 not in visited]
        if len(canidate_edges) == 0: break
        e = min(canidate_edges, key=lambda e: visited[e.v1] + e.weight)
        visited[e.v2] = visited[e.v1] + e.weight
        if next_hop[e.v1] == None:
            next_hop[e.v2] = e
        else:
            next_hop[e.v2] = next_hop[e.v1]

    return next_hop

def parse_input():
    f = open(sys.argv[1], 'r')
    g = Graph()
    seg = 1
    for line in f:
        v1, v2, w = [int(j) for j in line.split()]
        g.add_edge(v1, v2, seg, w)
        #g.add_edge(v2, v1, seg, w)
        seg += 1
    return g

def init_tables():
    config = {'tables': {}}
    config['tables']['ingress_table']      = {'rules': []}
    config['tables']['egress_table']       = {'rules': []}
    config['tables']['ipv4_routing_table'] = {'rules': []}
    config['tables']['cpu_hdr_table']      = {'rules': []}

    default_action = {'action': {'type': 'drop_packet'}, 'name': 'default'}
    config['tables']['ingress_table']['default_rule']      = default_action
    config['tables']['egress_table']['default_rule']       = default_action
    config['tables']['ipv4_routing_table']['default_rule'] = default_action

    default_action = {'action': {'type': 'no_operation'}, 'name': 'default'}
    config['tables']['cpu_hdr_table']['default_rule']      = default_action
    return config

def init_netflow_tables(config):
    default_action = {'name': 'default', 'action': {'type': 'clone_to_cpu'}}
    default_action['action']['data'] = {}
    default_action['action']['data']['cpu_port'] = {'value': CPU_PORT}
    default_action['action']['data']['reason']   = {'value': CPU_CODE_NETFLOW}
    config['tables']['ipv4_flow_table'] = {'default_rule': default_action}
    config['tables']['ipv6_flow_table'] = {'default_rule': default_action}
    return config

def gen_cpu_rules(config):
    rule = {'name': CPU_PORT, 'match': {}, 'action': {}}
    rule['match'] = { 'standard_metadata.egress_port': {'value': CPU_PORT}}
    rule['action'] = {'type': 'add_cpu_hdr'}
    config['tables']['cpu_hdr_table']['rules'].append(rule)
    return config

def gen_edge_rules(g, config):
    for e in g.edges:
        local_vrf = e.v1.id
        local_mac = "02:00:%02x:00:00:%02x" % (e.id, e.v1.id)
        remote_mac = "02:00:%02x:00:00:%02x" % (e.id, e.v2.id)
        logical_port = e.id
        physical_port =  ('p0','p4')[e.v1.id > e.v2.id]

        # (RX dest) MAC Address => set_ingress(vrf, rx_log_port)
        rule_name = "->V%d.%d" % (local_vrf, logical_port)
        rule = {'name': rule_name, 'match': {}, 'action': {}}
        rule['match'] = { 'ether_hdr.dst': {'value': local_mac}}
        rule['action'] = {'type': 'set_ingress', 'data': {}}
        rule['action']['data']['vrf'] = {'value': local_vrf}
        rule['action']['data']['rx_log_port'] = {'value': logical_port}
        config['tables']['ingress_table']['rules'].append(rule)

        # (This) VRF, (Next Hop) Inf => set_egress(smac, dmac, tx_phy_port)
        rule_name = "V%d.%d->" % (local_vrf, logical_port)
        rule = {'name': rule_name, 'match': {}, 'action': {}}
        rule['match']['local_metadata.vrf'] = {'value': local_vrf}
        rule['match']['local_metadata.tx_log_port'] = {'value': logical_port}
        rule['action'] = {'type': 'set_egress', 'data': {}}
        rule['action']['data']['smac'] = {'value': local_mac}
        rule['action']['data']['dmac'] = {'value': remote_mac}
        rule['action']['data']['tx_phy_port'] = {'value': physical_port}
        config['tables']['egress_table']['rules'].append(rule)

def gen_vertex_rules(g, config):
    for v1 in g.verticies:
        local_vrf = v1.id
        local_mac = "02:00:00:00:00:%02x" % (v1.id)
        remote_mac = "02:00:00:00:00:00"
        logical_port = 0
        physical_port = "v0.0"

        # (RX dest) MAC Address => set_ingress(vrf, rx_log_port)
        rule_name = "->V%d.%d" % (v1.id, logical_port)
        rule = {'name': rule_name, 'match': {}, 'action': {}}
        rule['match'] = { 'ether_hdr.dst': {'value': local_mac}}
        rule['action'] = {'type': 'set_ingress', 'data': {}}
        rule['action']['data']['vrf'] = {'value': local_vrf}
        rule['action']['data']['rx_log_port'] = {'value': logical_port}
        config['tables']['ingress_table']['rules'].append(rule)

        # (This) VRF, (Next Hop) Inf => set_egress(smac, dmac, tx_phy_port)
        rule_name = "V%d.%d->" % (v1.id, logical_port)
        rule = {'name': rule_name, 'match': {}, 'action': {}}
        rule['match']['local_metadata.vrf'] = {'value': local_vrf}
        rule['match']['local_metadata.tx_log_port'] = {'value': logical_port}
        rule['action'] = {'type': 'set_egress', 'data': {}}
        rule['action']['data']['smac'] = {'value': local_mac}
        rule['action']['data']['dmac'] = {'value': remote_mac}
        rule['action']['data']['tx_phy_port'] = {'value': physical_port}
        config['tables']['egress_table']['rules'].append(rule)

        nh = next_hops(g,v1)
        for v2 in nh:
            rule_name = "V%d->V%d" % (v1.id, v2.id)
            dest_prefix = "10.0.%d.0/24" % (v2.id)
            tx_log_port = nh[v2].id if nh[v2] != None else 0

            # (This) VRF, (Dest) IP => set_next_hop(tx_log_port)
            rule = {'name': rule_name, 'match': {}, 'action': {}}
            rule['match']['local_metadata.vrf'] = {'value': local_vrf}
            rule['match']['ipv4_hdr.dst'] = {'value': dest_prefix}
            rule['action'] = {'type': 'set_next_hop', 'data': {}}
            rule['action']['data']['tx_log_port'] = {'value': tx_log_port}
            config['tables']['ipv4_routing_table']['rules'].append(rule)

config = init_tables()
g = parse_input()
gen_edge_rules(g, config)
gen_vertex_rules(g, config)
init_netflow_tables(config)
gen_cpu_rules(config)
print json.dumps(config, indent=2)
