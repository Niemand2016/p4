#define FLOW_TABLE_SIZE 1024
#define CPU_PORT 9
#define CPU_EGRESS_SPEC 0

// Headers

#include "headers.p4"

header_type flow_metadata_t {
	fields {
		out_port  : 16;
		timestamp : 48;
		src_port  : 16;
		dst_port  : 16;
	}
}

header_type ipv4_flow_t {
	fields { // Length: 184 bits (23 Bytes)
		in_port   : 16;
		out_port  : 16;
        src_ip    : 32;
        dst_ip    : 32;
        proto     : 8;
		src_port  : 16;
		dst_port  : 16;
		timestamp : 48;
	}
}

header_type intrinsic_metadata_t {
    fields {
        ingress_global_timestamp : 48;
        lf_field_list : 8;
        mcast_grp : 16;
        egress_rid : 16;
        resubmit_flag : 8;
        recirculate_flag : 8;
    }
}

header_type l4_hdr_t {
	fields {
		src : 16;
		dst : 16;
	}
}

header   ether_hdr_t          ether_hdr;
header   ipv4_hdr_t           ipv4_hdr;
header   l4_hdr_t             l4_hdr;
header   ipv4_flow_t          ipv4_flow;
metadata intrinsic_metadata_t intrinsic_metadata;
metadata flow_metadata_t      flow_metadata;
// Field List

field_list metadata_fields {
	standard_metadata;
    intrinsic_metadata;
	flow_metadata;
}

// Parser

parser start {
	return ethernet;
}

parser ethernet {
	extract(ether_hdr);
	return select(ether_hdr.ethertype) {
		0x0800:	ipv4;
		//0x86dd: ipv6;
        0x88b5: ipv4_flow; // Should Never Occur, Needed to Compile
        //0x88b6: ipv6_flow;
		default: ingress;
	}
}

// This should never happen
parser ipv4_flow {
    extract(ipv4_flow);
    return ingress;
}

parser ipv4 {
	extract(ipv4_hdr);
	return select(ipv4_hdr.proto) {
		0x06: l4;
		0x11: l4;
		0x84: l4;
		default: ingress;
	}
}

parser l4 {
	extract(l4_hdr);
	set_metadata(flow_metadata.src_port, l4_hdr.src);
	set_metadata(flow_metadata.dst_port, l4_hdr.dst);
	return ingress;
}

// Actions

action drop_packet() {
	drop();
}

action set_egr(port) {
	modify_field(standard_metadata.egress_spec, port);
	modify_field(flow_metadata.out_port, port);
	modify_field(flow_metadata.timestamp, intrinsic_metadata.ingress_global_timestamp);
}

action update_ipv4_flow(index) {
    register_write(ipv4_last_timestamp, index, intrinsic_metadata.ingress_global_timestamp);
}

action add_flow() {
	clone_ingress_pkt_to_egress(CPU_EGRESS_SPEC, metadata_fields);
}

action build_ipv4_flow() {
    add_header(ipv4_flow);
	modify_field(ipv4_flow.in_port,   standard_metadata.ingress_port);
	modify_field(ipv4_flow.out_port,  flow_metadata.out_port);
    modify_field(ipv4_flow.src_ip,    ipv4_hdr.src);
    modify_field(ipv4_flow.dst_ip,    ipv4_hdr.dst);
    modify_field(ipv4_flow.proto,     ipv4_hdr.proto);
    modify_field(ipv4_flow.src_port,  flow_metadata.src_port);
    modify_field(ipv4_flow.dst_port,  flow_metadata.dst_port);
    modify_field(ipv4_flow.timestamp, flow_metadata.timestamp);
    remove_header(ether_hdr);
    remove_header(ipv4_hdr);
    remove_header(l4_hdr);
    truncate(23);
}

// Tables

table ipv4_flows {
	reads {
		standard_metadata.ingress_port : exact;
		standard_metadata.egress_spec : exact;
		ipv4_hdr.src           : exact;
		ipv4_hdr.dst           : exact;
		ipv4_hdr.proto         : exact;
		l4_hdr.src : exact;
		l4_hdr.dst : exact;
	}
	actions {
		update_ipv4_flow;
		add_flow;
	}
    size: FLOW_TABLE_SIZE;
}

table forward {
	reads {
		standard_metadata.ingress_port : exact;
	}
	actions {
		set_egr;
		drop_packet;
	}
    min_size: 2;
}

table build {
	reads {
        standard_metadata.egress_port : exact;
	}
	actions {
		build_ipv4_flow;
	}
    min_size: 1;
}

// Registers

register ipv4_first_timestamp {
    width : 48;
    static: ipv4_flows;
    instance_count: FLOW_TABLE_SIZE;
}

register ipv4_last_timestamp {
    width : 48;
    static: ipv4_flows;
    instance_count: FLOW_TABLE_SIZE;
}

// Control

control ingress {
	apply(forward);
	if (valid(ipv4_hdr)) { apply(ipv4_flows); }
}

control egress {
    apply(build);
}
