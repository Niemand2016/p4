#!/usr/bin/python
NETFLOW_DIGEST_APP_ID = 5

import sys
import time
import json
sys.path.append('/home/joseph/Documents/p4/nfp-thrift/gen-py')

from thrift.transport import TTransport
from thrift.transport import TZlibTransport
from thrift.transport import TSocket
#from thrift.transport import TSSLSocket
from thrift.transport import THttpClient
from thrift.protocol import TBinaryProtocol

from sdk6_rte import RunTimeEnvironment
from sdk6_rte.ttypes import *

class NFP:
    def __init__(self, host, port):
        self.table_entry_counter = {}
        socket = TSocket.TSocket(host, port)
        self.transport = TTransport.TBufferedTransport(socket)

        # May need to disable
        self.transport = TZlibTransport.TZlibTransport(self.transport)

        protocol = TBinaryProtocol.TBinaryProtocol(self.transport)
        self.client = RunTimeEnvironment.Client(protocol)
        self.transport.open()

    def register_listener(self, app_id):
        digest_id = -1
        digests = self.client.digest_list_all()

        for d in digests:
            if d.app_id == app_id:
                digest_id = d.id

        if digest_id < 0:
            return None
        else:
            return self.client.digest_register(digest_id)

    def add_entry(self, tbl_id, table_entry):
        if tbl_id in self.table_entry_counter.keys():
            self.table_entry_counter[tbl_id] += 1
        else:
            self.table_entry_counter[tbl_id] = 1
        table_entry.rule_name = str(self.table_entry_counter[tbl_id])
        self.client.table_entry_add(tbl_id, table_entry)

    def poll_digest(self, dh):
        msgs = []
        values = self.client.digest_retrieve(dh)
        for i in range(0,len(values),6):
            msgs.append(values[i:i+6])
        return msgs

    def get_table_id(self, table_name):

        tl = self.client.table_list_all()
        for t in tl:
            if t.tbl_name == table_name:
                return t.tbl_id

    def disconnect(self):
        self.transport.close()

def ipv4_flow_to_table_entry(flow):
    actions = {'type': 'no_action'}
    match = {}
    match['standard_metadata.ingress_port'] = {'value': flow[0]}
    match['ipv4_hdr.src'] = {'value': flow[1]}
    match['ipv4_hdr.dst'] = {'value': flow[2]}
    match['ipv4_hdr.proto'] = {'value': flow[3]}
    match['netflow_metadata.src_port'] = {'value': flow[4]}
    match['netflow_metadata.dst_port'] = {'value': flow[5]}
    table_entry = TableEntry()
    table_entry.default_rule = False
    table_entry.actions = json.dumps(actions)
    table_entry.match = json.dumps(match)
    table_entry.priority = 0
    return table_entry

nfp = NFP('nitrogen.sdn.surfnet.nl', 20206)
table_id = nfp.get_table_id('ipv4_flow_table')
dh = nfp.register_listener(NETFLOW_DIGEST_APP_ID)

for t in range(60):
    flows = []
    while not len(flows):
        flows = nfp.poll_digest(dh)
        time.sleep(1)

    for flow in flows:
        table_entry = ipv4_flow_to_table_entry(flow)
        print flow
        print table_entry
        nfp.add_entry(table_id, table_entry)

nfp.disconnect()
