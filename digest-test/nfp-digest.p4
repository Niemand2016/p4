#define STANDARD_PARSER_RETURN ingress
#define NETFLOW_CONTROL_RETURN ingress_final
#define LOGICAL_IN_PORT standard_metadata.ingress_port
#define LOGICAL_OUT_PORT standard_metadata.egress_port
#define LOGICAL_PORT_SIZE 8

#include "../common/standard_parser.p4"
#include "../common/netflow.p4"
#include "../common/standard_actions.p4"

parser start {
	return standard_parser;
}

// Headers

// Field List

// Actions

action set_egress(port) {
    modify_field(standard_metadata.egress_spec, port);
}

// Tables

table egress_table {
    reads {
		ether_hdr.dst : exact;
    }
	actions {
        set_egress;
        drop_packet;
	}
    size : 1024;
}

// Control

control ingress {
    netflow();
    apply(egress_table);
    
}

control ingress_final {}

control egress {}

control egress_final {}
