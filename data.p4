#define FLOW_TABLE_SIZE 1024
#define CPU_PORT 9
#define CPU_EGRESS_SPEC 0

// Headers

#include "headers.p4"

header_type flow_metadata_t {
	fields {
		src_port  : 16;
		dst_port  : 16;
        checksum  : 16;
	}
}

header_type ipv4_flow_t {
	fields { // Length: 168 bits (21 Bytes)
        ethertype : 16;
        src_ip    : 32;
        dst_ip    : 32;
        proto     : 8;
		src_port  : 16;
		dst_port  : 16;
		timestamp : 48;
	}
}

header_type ipv6_flow_t {
	fields { // Length: 360 bits (45 Bytes)
        ethertype : 16;
        src_ip    : 128;
        dst_ip    : 128;
        proto     : 8;
		src_port  : 16;
		dst_port  : 16;
		timestamp : 48;
	}
}

header_type intrinsic_metadata_t {
    fields {
        ingress_global_timestamp : 48;
        lf_field_list : 8;
        mcast_grp : 16;
        egress_rid : 16;
        resubmit_flag : 8;
        recirculate_flag : 8;
    }
}



header   ether_hdr_t          ether_hdr;
header   ipv4_hdr_t           ipv4_hdr;
header   ipv6_hdr_t           ipv6_hdr;
header   tcp_hdr_t            tcp_hdr;
header   udp_hdr_t            udp_hdr;
header   ipv4_flow_t          ipv4_flow;
header   ipv6_flow_t          ipv6_flow;
metadata intrinsic_metadata_t intrinsic_metadata;
metadata flow_metadata_t      flow_metadata;

// Field List

field_list metadata_fields {
	standard_metadata;
    intrinsic_metadata;
}

field_list bloom_fields {
    ether_hdr.ethertype;
    ipv4_hdr.src;
    ipv4_hdr.dst;
    ipv6_hdr.src;
    ipv6_hdr.dst;
    udp_hdr.src;
    udp_hdr.dst;
    tcp_hdr.src;
    tcp_hdr.dst;
}

field_list_calculation bloom_index_1 {
    input {
        bloom_fields;
    }
    algorithm : crc16;
    output_width : 16;
}

//calculated_field flow_metadata.checksum {
//    update bloom_index_1;
//}

// Parser

parser start {
	return ethernet;
}

parser ethernet {
	extract(ether_hdr);
	return select(ether_hdr.ethertype) {
		0x0800:	ipv4;
		0x86dd: ipv6;
        0x88b5: ipv4_flow;
        0x88b6: ipv6_flow;
		default: ingress;
	}
}

// This should never happen
parser ipv4_flow {
    extract(ipv4_flow);
    return ingress;
}

// This should never happen
parser ipv6_flow {
    extract(ipv6_flow);
    return ingress;
}

parser ipv4 {
	extract(ipv4_hdr);
	return select(ipv4_hdr.proto) {
		0x06: tcp;
		0x11: udp;
		default: ingress;
	}
}

parser ipv6 {
	extract(ipv6_hdr);
	return select(ipv6_hdr.next_hdr) {
		0x06: tcp;
		0x11: udp;
		default: ingress;
	}
}

parser tcp {
	extract(tcp_hdr);
	set_metadata(flow_metadata.src_port, tcp_hdr.src);
	set_metadata(flow_metadata.dst_port, tcp_hdr.dst);
	return ingress;
}

parser udp {
	extract(udp_hdr);
	set_metadata(flow_metadata.src_port, udp_hdr.src);
	set_metadata(flow_metadata.dst_port, udp_hdr.dst);
	return ingress;
}

// Actions

action drop_packet() {
	drop();
}

action set_egr(port) {
	modify_field(standard_metadata.egress_spec, port);
}

action update_ipv4_flow(index) {
    register_write(ipv4_timestamp, index, intrinsic_metadata.ingress_global_timestamp);
}

action update_ipv6_flow(index) {
    register_write(ipv6_timestamp, index, intrinsic_metadata.ingress_global_timestamp);
}

action add_flow() {
	clone_ingress_pkt_to_egress(CPU_EGRESS_SPEC, metadata_fields);
}

action build_ipv4_flow() {
    add_header(ipv4_flow);
    modify_field(ipv4_flow.ethertype, ether_hdr.ethertype);
    modify_field(ipv4_flow.src_ip,    ipv4_hdr.src);
    modify_field(ipv4_flow.dst_ip,    ipv4_hdr.dst);
    modify_field(ipv4_flow.proto,     ipv4_hdr.proto);
    modify_field(ipv4_flow.src_port,  flow_metadata.src_port);
    modify_field(ipv4_flow.dst_port,  flow_metadata.dst_port);
    modify_field(ipv4_flow.timestamp, intrinsic_metadata.ingress_global_timestamp);
    remove_header(ether_hdr);
    remove_header(ipv4_hdr);
    remove_header(tcp_hdr);
    remove_header(udp_hdr);
    truncate(21);
}

action build_ipv6_flow() {
    add_header(ipv6_flow);
    modify_field(ipv6_flow.ethertype, ether_hdr.ethertype);
    modify_field(ipv6_flow.src_ip,    ipv6_hdr.src);
    modify_field(ipv6_flow.dst_ip,    ipv6_hdr.dst);
    modify_field(ipv6_flow.proto,     ipv6_hdr.next_hdr);
    modify_field(ipv6_flow.src_port,  flow_metadata.src_port);
    modify_field(ipv6_flow.dst_port,  flow_metadata.dst_port);
    modify_field(ipv6_flow.timestamp, intrinsic_metadata.ingress_global_timestamp);
    remove_header(ether_hdr);
    remove_header(ipv6_hdr);
    remove_header(tcp_hdr);
    remove_header(udp_hdr);
    truncate(45);
}

action update_bloom() {
    modify_field_with_hash_based_offset(flow_metadata.checksum, 0, bloom_index_1, 65536);
    register_write(bloom_hash, 0, flow_metadata.checksum);
}

// Tables

table ipv4_flows {
	reads {
		ipv4_hdr.src           : exact;
		ipv4_hdr.dst           : exact;
		ipv4_hdr.proto         : exact;
		flow_metadata.src_port : exact;
		flow_metadata.dst_port : exact;
	}
	actions {
		update_ipv4_flow;
		add_flow;
	}
    size: FLOW_TABLE_SIZE;
}

table ipv6_flows {
	reads {
		ipv6_hdr.src           : exact;
		ipv6_hdr.dst           : exact;
		ipv6_hdr.next_hdr      : exact;
		flow_metadata.src_port : exact;
		flow_metadata.dst_port : exact;
	}
	actions {
		update_ipv6_flow;
		add_flow;
	}
    size: FLOW_TABLE_SIZE;
}

table forward {
	reads {
		standard_metadata.ingress_port : exact;
	}
	actions {
		set_egr;
		drop_packet;
	}
    min_size: 2;
}

table build {
	reads {
        ether_hdr.ethertype : exact;
	}
	actions {
		build_ipv4_flow;
        build_ipv6_flow;
		drop_packet;
	}
    min_size: 2;
}

table bloom_table {
    reads {
        flow_metadata.checksum : exact;
    }
    actions {
        update_bloom;
    }
}

// Registers

register bloom_hash {
    width : 16;
    static : bloom_table;
    instance_count : 1;
}

register ipv4_timestamp {
    width : 48;
    static: ipv4_flows;
    instance_count: FLOW_TABLE_SIZE;
}

register ipv6_timestamp {
    width : 48;
    static: ipv6_flows;
    instance_count: FLOW_TABLE_SIZE;
}

// Control

control ingress {
	if (valid(ipv4_hdr)) { apply(ipv4_flows); }
	if (valid(ipv6_hdr)) { apply(ipv6_flows); }
 	apply(forward);
}

control egress {
    if (standard_metadata.egress_port == CPU_PORT) {
	    apply(build);
    } else {
        apply(bloom_table);
    }
}
