#!/bin/bash

function config_inf {
    echo ip link set dev $1 up
    echo ip addr flush dev $1
}

function create_pair {
    echo ip link add name veth${1}h type veth peer name veth${1}p
    config_inf veth${1}h
    config_inf veth${1}p
}

function create_pairs {
    for i in $@
    do
        create_pair $i
    done
}

create_pairs $@
