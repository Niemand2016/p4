#!/bin/sh

ip link add name veth0 type veth peer name p4eth0
ip link add name veth1 type veth peer name p4eth1
ip link set dev veth0 up
ip link set dev p4eth0 up
ip link set dev veth1 up
ip link set dev p4eth1 up

sleep 2
ip addr flush dev veth0
ip addr flush dev p4eth0
ip addr flush dev veth1
ip addr flush dev p4eth0
