header_type ether_hdr_t {
	fields {
		dst       : 48;
		src       : 48;
		ethertype : 16;
	}
}
header ether_hdr_t ether_hdr;

parser start {
	extract(ether_hdr);
    return ingress;
}

field_list clone_fields {
	standard_metadata.ingress_port;
}

action not_clone(egress_port) {
    clone_ingress_pkt_to_ingress(0, clone_fields);
    modify_field(standard_metadata.egress_spec, egress_port);
    modify_field(ether_hdr.dst, 0x111111111111);
}

action is_clone(egress_port) {
    modify_field(standard_metadata.egress_spec, egress_port);
    modify_field(ether_hdr.dst, 0x222222222222);
}

table clone_check {
    reads {
        standard_metadata.instance_type : exact;
    }
    actions {
        is_clone;
        not_clone;
    }
}

control ingress {
    apply(clone_check);
}

control egress {

}
