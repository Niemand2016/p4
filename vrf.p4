// Headers

#include "headers.p4"

#define CPU_PORT 9
#define DROP_PORT 0

header_type intrinsic_metadata_t {
    fields {
        ingress_global_timestamp : 48;
        lf_field_list            : 8;
        mcast_grp                : 16;
        egress_rid               : 16;
        resubmit_flag            : 8;
        recirculate_flag         : 8;
    }
}

header_type local_metadata_t {
	fields {
		vrf      : 8;
		next_hop : 8;
        log_port : 8;
	}
}

header   ether_hdr_t          ether_hdr;
header   ipv4_hdr_t           ipv4_hdr;
header   ipv6_hdr_t           ipv6_hdr;
metadata intrinsic_metadata_t intrinsic_metadata;
metadata local_metadata_t     local_metadata;

// Parser

parser_exception pe_unsupported_protocol {
    parser_drop;
}

parser start {
	return ethernet;
}

parser ethernet {
	extract(ether_hdr);
	return select(ether_hdr.ethertype) {
		0x0800  : ipv4;
//		0x86dd  : ipv6;
		default : parse_error pe_unsupported_protocol;
	}
}

parser ipv4 {
	extract(ipv4_hdr);
	return select(ipv4_hdr.proto) {
//		0x06    : tcp;
//		0x11    : udp;
//		0x84    : sctp;
//		0xfd    : Experimental 1
//		0xfe    : Experimental 2
		default : ingress;
	}
}

parser ipv6 {
	extract(ipv6_hdr);
	return select(ipv6_hdr.next_hdr) {
//		0x06    : tcp;
//		0x11    : udp;
//		0x84    : sctp;
//		0xfd    : Experimental 1
//		0xfe    : Experimental 2
		default : ingress;
	}
}

// Actions

action drop_packet() {
	drop();
}

action set_vrf(vrf) {
	modify_field(local_metadata.vrf, vrf);
}

action send_to_cpu() {
    modify_field(standard_metadata.egress_spec, CPU_PORT);
}

action set_next_hop(next_hop, log_port, phy_port) {
    modify_field(local_metadata.next_hop, next_hop);
    modify_field(local_metadata.log_port, log_port);
    modify_field(standard_metadata.egress_spec, phy_port);
}

action set_smac(smac) {
    modify_field(ether_hdr.src, smac);
}

action set_dmac(dmac) {
    modify_field(ether_hdr.dst, dmac);
}



// Tables

table vrf_table {
	reads {
		ether_hdr.dst : exact;
	}
	actions {
		set_vrf;
		drop_packet;
	}
}

table ipv4_routing_table {
	reads {
		local_metadata.vrf : exact;
		ipv4_hdr.dst       : lpm;
	}
	actions {
		set_next_hop;
        send_to_cpu;
		drop_packet;
	}
}

table smac_table {
    reads {
        local_metadata.vrf      : exact;
        local_metadata.log_port : exact;
    }
	actions {
        set_smac;
		drop_packet;
	}
}

table dmac_table {
    reads {
        local_metadata.vrf      : exact;
        local_metadata.next_hop : exact;
    }
	actions {
        set_dmac;
		drop_packet;
	}
}

// Registers

// Control

control ingress {
	apply(vrf_table);
	if (valid(ipv4_hdr)) { apply(ipv4_routing_table); }
}

control egress {
    if (standard_metadata.egress_port != CPU_PORT) {
        apply(smac_table);
        apply(dmac_table);
    }
}
