#!/usr/bin/python

import sys

from scapy.all import sniff

sys.path.append('gen-py')

from bm_runtime.standard import Standard
from bm_runtime.standard.ttypes import *

from sswitch_runtime import SimpleSwitch
from sswitch_runtime.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TMultiplexedProtocol

def connect():
    transport = TSocket.TSocket('127.0.0.1', 9090)
    transport = TTransport.TBufferedTransport(transport)
    transport.open()
    return transport;

def get_client(transport):
    bProtocol = TBinaryProtocol.TBinaryProtocol(transport)
    protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'standard')
    return Standard.Client(protocol)

def init_switch(client):
    client.bm_mt_set_default_action(0,'flow','add_flow',None)

def add_flow(pkt, client):
    dst = str(pkt)[0:6]
    src = str(pkt)[6:12]

    match_key = []
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(src)))
    match_key.append(BmMatchParam(type=BmMatchParamType.EXACT, exact=BmMatchParamExact(dst)))
    client.bm_mt_add_entry(0, 'flow', match_key, 'update_flow', None, None)

connection = connect()
client = get_client(connection)
init_switch(client)

sniff(iface='veth1', prn=lambda x: add_flow(x, client))

connection.close()
