#define STANDARD_PARSER_RETURN ingress
#define DIGEST_RECV_DPL 3
#define DPL_ETHERTYPE 0x88b5
#include "dpl_parser.p4"
#include "../common/standard_actions.p4"

parser start {
    return ethernet;
}

// Headers

header_type vrf_metadata_t {
    fields {
        id      : 8;
        rx_port : 8;
        tx_port : 8;
    }
}
metadata vrf_metadata_t vrf_metadata;

header_type dpl_metadata_t {
    fields {
        src       : 8;
        lbl       : 8;
        ethertype : 16;
    }
}
metadata dpl_metadata_t dpl_metadata;

// Field List

field_list dpl_fields {
	vrf_metadata.id;
	vrf_metadata.rx_port;
	dpl_metadata.src;
	dpl_metadata.lbl;
}

// Actions

action set_ingress(vrf_id, vrf_rx_port) {
    modify_field(vrf_metadata.id, vrf_id);
    modify_field(vrf_metadata.rx_port, vrf_rx_port);
}

action set_next_hop(vrf_tx_port) {
    modify_field(vrf_metadata.tx_port, vrf_tx_port);
}

action set_egress(smac, dmac, egress_port) {
    modify_field(ether_hdr.src, smac);
    modify_field(ether_hdr.dst, dmac);
    modify_field(standard_metadata.egress_spec, egress_port);
}

action swap_label(new_lbl) {
    modify_field(dpl_metadata.src, vrf_metadata.id);
    modify_field(dpl_metadata.lbl, new_lbl);
}

action generate_label() {
    generate_digest(DIGEST_RECV_DPL, dpl_fields);
    set_untrackable();
}

action set_untrackable() {
    modify_field(dpl_metadata.src, 255);
    modify_field(dpl_metadata.lbl, 255);
}

action add_dpl_tag() {
    add_header(dpl_hdr);
    modify_field(ether_hdr.ethertype, DPL_ETHERTYPE);
    modify_field(dpl_hdr.src, dpl_metadata.src);
    modify_field(dpl_hdr.lbl, dpl_metadata.lbl);
    modify_field(dpl_hdr.ethertype, dpl_metadata.ethertype);
}

action remove_dpl_tag() {
    remove_header(dpl_hdr);
    modify_field(ether_hdr.ethertype, dpl_metadata.ethertype);
}

// Tables

table dpl_tag_table {
    reads {
        standard_metadata.egress_port : exact;
    }
    actions {
        add_dpl_tag;
        remove_dpl_tag;
    }
}

table ingress_table {
    reads {
        ether_hdr.dst : exact;
    }
    actions {
        set_ingress;
        drop_packet;
    }
}

table ipv4_routing_table {
    reads {
        vrf_metadata.id : exact;
        ipv4_hdr.dst    : lpm;
    }
    actions {
        set_next_hop;
        drop_packet;
    }
}

table egress_table {
    reads {
        vrf_metadata.id      : exact;
        vrf_metadata.tx_port : exact;
    }
    actions {
        set_egress;
        drop_packet;
    }
}

table dpl_table {
    reads {
        vrf_metadata.id      : exact;
        vrf_metadata.rx_port : exact;
        dpl_metadata.src     : exact;
        dpl_metadata.lbl     : exact;
    }
    actions {
        swap_label;
        generate_label;
        no_action;
    }
}
// Determines which ports goto the control plane and therforee recieve a
// control plane header.


// Control

control ingress {
    apply(ingress_table);
    if (valid(ipv4_hdr)) { apply(ipv4_routing_table); }
    apply(egress_table);
}

control egress {
    apply(dpl_table);
    apply(dpl_tag_table);
}
