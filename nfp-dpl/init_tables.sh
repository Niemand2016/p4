#!/bin/bash

HOST=nitrogen.sdn.surfnet.nl
PORT=20206

cd /home/joseph/Documents/p4/nfp-sdk-6.0.2/p4/bin/

./rtecli -r $HOST -p $PORT tables -t dpl_tag_table -r add_tag -d \
-a '{  "type" : "add_dpl_tag",  "data" : {  } }' add

./rtecli -r $HOST -p $PORT tables -t dpl_tag_table -r remove_tag \
-m '{ "standard_metadata.egress_port" : {  "value" : "v0.0" } }' \
-a '{  "type" : "remove_dpl_tag",  "data" : {  } }' add

./rtecli -r $HOST -p $PORT tables -t dpl_table -r gen_label -d \
-a '{  "type" : "generate_label",  "data" : {  } }' add
